package com.example.iths.fuckingamazingprojectbro.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.example.iths.fuckingamazingprojectbro.service.LoginService;

/**
 * Created by Andreas Hansson on 2015-12-01.
 * This will be a parent for all the activities that will logout when a
 * logout-intent is sent.
 *
 * Glorious victory - a short story about panic and relief
 * Written by Christian Nordmark
 *
 * With the dark lord Deadlion on their heels, breathing down their necks
 * our mighty warriors noticed a flaw in their plan.
 *
 * - "Guys, this isn't working...", said Bartek the bard in a sing-song voice.
 * - "What do you mean it's not working?", a choir of voices echoed back at him.
 * - "The logout spell is acting up and we don't have a plan to back us up", Bartek continued the song.
 *
 * Andreahs the Andal and the shadow priest Christian gathered around the bard, sceptically eyeing
 * the magical device in his hands. They had just begun to understand the properties of this... box of sorts
 * and all of them knew that, at any moment, something could go very wrong very quickly.
 * And it had.
 *
 * Jag får inte skriva mer här nu säger resten av min grupp.
 * Vad som hade hänt i resten av storyn är iaf att gruppen hade fått en snilleblixt och använt sig av
 * lite ful magi för att besegra Deadlion. Det hela skulle, obviously, vara en analogi för hur
 * vi precis innan inlämning fick problem med vår logout på Barteks telefon (kanske p.g.a android 6)
 * och löste det på ett väldigt fult sätt som i stort sett gör hela den här klassen meningslös.
 * Men whatever.
 */
public class BaseActivity extends AppCompatActivity {

    public static boolean logoutHack = false;
    public static final String LOG_OUT = "event_logout";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register mMessageReceiver to receive messages.
        if(logoutHack)
            finish();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
            new IntentFilter(LOG_OUT));
    }

    // handler for received Intents for logout event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do your code snippet here.
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    /**
     * This will send a logout-intent to all activities that inherit this activity.
     * @param context - the context of the activity that will use this method.
     */
    public static void logout(Context context) {
        //do this on logout button click
        LoginService.setCurrentEmployee();
        logoutHack = true;
        Intent intent = new Intent(LOG_OUT);

        //send the broadcast to all activities who are listening
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
