package com.example.iths.fuckingamazingprojectbro.activity.interfaces;

import com.example.iths.fuckingamazingprojectbro.data.Order;
import java.util.ArrayList;

/**
 * Created by iths on 2015-12-07.
 */
public interface EmployeeOrdersServiceCallback {
    public void EmployeeOrdersServiceGetFailed(Exception e);
    public void EmployeeOrdersServiceGetSuccess(ArrayList<Order> orders);
}
