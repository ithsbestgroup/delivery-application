package com.example.iths.fuckingamazingprojectbro.service;

import android.os.AsyncTask;

import com.example.iths.fuckingamazingprojectbro.activity.interfaces.LoginServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.AbstractEmployee;
import com.example.iths.fuckingamazingprojectbro.data.Driver;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Service class that handles login attempts and acts as a global accesspoint to a singleton
 * employee object that can be used to store the logged in user after successful login.
 */
public class LoginService {

    private LoginServiceCallback callback;
    private Exception error;

    private static AbstractEmployee currentEmployee;

    /**
     * Default construct
     * @param callback - The activity that uses this service
     */
    public LoginService(LoginServiceCallback callback){
        this.callback = callback;
    }

    /**
     * Make a login attempt
     * @param id - Id to attempt to login
     * @param password - The fucking password to attempt. Come on, brah -.-
     */
    public void login(final String id, final String password) {
        new AsyncTask<String, Void, String>() {
            protected String doInBackground(String... strings) {
                try {
                    String charset = "UTF-8";
                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(id, charset),
                            URLEncoder.encode(password, charset));

                    URL url = new URL("http://apic1.udda.net/androidproject/rest/employees/");

                    //Set connection settings and request properties
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                    //Write the post data
                    OutputStream output = connection.getOutputStream();
                    output.write(query.getBytes());

                    InputStream is = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    //Loop throgh response and build a string from it
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    return result.toString();

                } catch (Exception e) {
                    error = e;
                }
                //Returns to onPostExecute
                return null;
            }
            protected void onPostExecute(String string){
                //If something went wrong in doInBackground we catch it here
                if(string == null && error != null){
                    callback.LoginServiceFailed(error);
                    return;
                }
                try {

                    //Used to check if string from api is JSONObject or JSONArray
                    JSONObject json = (JSONObject) new JSONTokener(string).nextValue();

                    Driver driver = new Driver();
                    driver.populate(json);
                    if(driver.getFirstName().equals("")){
                        throw new JSONException("User was not able to log in correctly");
                    }
                    else {
                        callback.LoginServiceSuccess(driver);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.LoginServiceFailed(e);
                }
            }
        }.execute();
    }

    /**
     * Saves a singleton employee for global access
     * @param employee - The employee to save
     */
    public static void setCurrentEmployee(AbstractEmployee employee){
        LoginService.currentEmployee = employee;
    }

    /**
     * Sets the singleton employee to null
     * Aka removes it yo
     */
    public static void setCurrentEmployee(){
        LoginService.currentEmployee = null;
    }

    //Gets the singleton employee
    public static AbstractEmployee getCurrentEmployee(){
        return LoginService.currentEmployee;
    }

}
