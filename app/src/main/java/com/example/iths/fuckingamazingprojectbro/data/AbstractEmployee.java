package com.example.iths.fuckingamazingprojectbro.data;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for building different types of employees on
 */


public abstract class AbstractEmployee implements JSONPopulatable{
    private String firstName;
    private String lastName;
    private int id;
    private String passWord;
    private HashMap<String, String> settings;

    /**
     * Instantiate method (only able to instantiate the extented classes)
     */
    public AbstractEmployee(){
        this.settings = new HashMap<>();
    }

    public String getSetting(String name){
        return settings.get(name);
    }
    public void setSetting(String name, String value){
        settings.put(name, value);
    }
    /**
     * Method for returning the employees first name
     * @return the first name of the employee
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the employee
     * @param firstName the first name of the employee
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Method for returning the last name of the employee
     * @return returns the last name of the employee
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method for setting the last name of the employee
     * @param lastName the last name of the employee
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter method for returning the ID of the employee
     * @return the ID of the employee
     */

    public int getId() {
        return id;
    }

    /**
     * Setter method for setting the ID of the employee
     * @param id the ID of the employee
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter method for returning the employees password
     * @return the employees password
     */
    public String getPassWord() {
        return passWord;
    }

    /**
     * Setter method for setting the employees password
     * @param passWord the employees password
     */
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    /**
     * Method for returning the employees orders
     * @return the employees orders
     */
    public abstract ArrayList<Order> getOrders();
}
