package com.example.iths.fuckingamazingprojectbro.data;


import org.json.JSONObject;

import java.io.Serializable;

import java.util.Comparator;

/**
 * Created by iths on 15-11-18.
 */
public class Order implements Serializable, JSONPopulatable{

    private int id;
    private int employeeId;
    private String createdAt;
    private String deliveredAt;
    private String deliveryContactPerson;
    private String deliveryEmailAdress;
    private String deliveryPhoneNumber;
    private String deliveryStreetAdress;
    private String deliveryCity;
    private int deliveryZipCode;
    private String deliveryLatitude;
    private String deliveryLongitude;
    private boolean selected;

    /**
     *
     * @returns the order's id-number.
     */
    public int getId() {
        return this.id;
    }

    /**
     * This will be used to set the orders id-number.
     * @param id - the order's id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @returns the id of the employee that is connected to this order.
     */
    public int getEmployeeId() {
        return this.employeeId;
    }

    /**
     * This will set the current employees id that is connected to this order.
     * @param employeeId - the current employee's id.
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    /**
     *
     * @returns a timestamp of when this order was created.
     */
    public String getCreatedAt() {
        return this.createdAt;
    }

    /**
     * This will set the timestamp of when this order was created.
     * @param createdAt - the timestamp as a string.
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @returns the timestamp of when this order was delivered.
     */
    public String getDeliveredAt() {
        return this.deliveredAt;
    }

    /**
     * This will set the timestamp of when this order was delivered.
     * @param deliveredAt - the timestamp as a string.
     */
    public void setDeliveredAt(String deliveredAt) {
        this.deliveredAt = deliveredAt;
    }

    /**
     *
     * @returns the contact person for the order.
     */
    public String getDeliveryContactPerson() {
        return this.deliveryContactPerson;
    }

    /**
     * This will set the contact person for the order.
     * @param deliveryContactPerson - the contact person.
     */
    public void setDeliveryContactPerson(String deliveryContactPerson) {
        this.deliveryContactPerson = deliveryContactPerson;
    }

    /**
     *
     * @returns the email-address of the contact person for this order.
     */
    public String getDeliveryEmailAdress() {
        return this.deliveryEmailAdress;
    }

    /**
     * This will set the contact person's email address.
     * @param deliveryEmailAdress - the contact person's email-address.
     */
    public void setDeliveryEmailAdress(String deliveryEmailAdress) {
        this.deliveryEmailAdress = deliveryEmailAdress;
    }

    /**
     *
     * @returns the contact person's phone number as a string.
     */
    public String getDeliveryPhoneNumber() {
        return this.deliveryPhoneNumber;
    }

    /**
     * This will set the contact person's phone number.
     * @param deliveryPhoneNumber - the contact person's phone number as a string.
     */
    public void setDeliveryPhoneNumber(String deliveryPhoneNumber) {
        this.deliveryPhoneNumber = deliveryPhoneNumber;
    }

    /**
     *
     * @returns the street-address where the order will be delivered at.
     */
    public String getDeliveryStreetAdress() {
        return this.deliveryStreetAdress;
    }

    /**
     * This will set the street-address where the order will be delivered at.
     * @param deliveryStreetAdress - the street address.
     */
    public void setDeliveryStreetAdress(String deliveryStreetAdress) {
        this.deliveryStreetAdress = deliveryStreetAdress;
    }

    /**
     *
     * @returns the city where this order will be delivered at.
     */
    public String getDeliveryCity() {
        return this.deliveryCity;
    }

    /**
     * This will set the city where the order will be delivered at.
     * @param deliveryCity - the city.
     */
    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    /**
     *
     * @returns the zip-code where this order will be delivered at.
     */
    public int getDeliveryZipCode() {
        return this.deliveryZipCode;
    }

    /**
     * This will set the zip-code of where this order will be delivered at.
     * @param deliveryZipCode - the zip-code.
     */
    public void setDeliveryZipCode(int deliveryZipCode) {
        this.deliveryZipCode = deliveryZipCode;
    }

    /**
     *
     * @returns the latitude-position of where the order will be delivered at.
     */
    public String getDeliveryLatitude() {
        return this.deliveryLatitude;
    }

    /**
     * This will set the latitude-position of where the order will be delivered at.
     * @param deliveryLatitude - the latitude-position as a string.
     */
    public void setDeliveryLatitude(String deliveryLatitude) {
        this.deliveryLatitude = deliveryLatitude;
    }

    /**
     *
     * @returns the longitude-position of where the order will be delivered at.
     */
    public String getDeliveryLongitude() {
        return this.deliveryLongitude;
    }

    /**
     * This will set the longitude-position of where the order will be delivered at.
     * @param deliveryLongitude - the longitude-position as a string.
     */
    public void setDeliveryLongitude(String deliveryLongitude) {
        this.deliveryLongitude = deliveryLongitude;
    }

    /**
     *
     * @returns true if an item is selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * If an item is selected, then set this to true.
     * @param selected - boolean value for if the item is selected or not.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * This will sort the orders in an ascending id order.
     */
    public static Comparator<Order> orderIdComparator = new Comparator<Order>() {
        @Override
        public int compare(Order o1, Order o2) {
            int orderId1 = o1.getId();
            int orderId2 = o2.getId();

            return orderId1-orderId2;
        }
    };

    /**
     * This will sort the orders in an ascending zip-code order.
     */
    public static Comparator<Order> orderZipComparator = new Comparator<Order>() {
        @Override
        public int compare(Order o1, Order o2) {
            int orderZip1 = o1.getDeliveryZipCode();
            int orderZip2 = o2.getDeliveryZipCode();

            return orderZip1-orderZip2;
        }
    };

    /**
     * This will sort the orders in an ascending address order.
     */
    public static Comparator<Order> orderAddressComparator = new Comparator<Order>() {
        @Override
        public int compare(Order o1, Order o2) {
            String orderAddress1 = o1.getDeliveryStreetAdress().toUpperCase();
            String orderAddress2 = o2.getDeliveryStreetAdress().toUpperCase();

            return orderAddress1.compareTo(orderAddress2);
        }
    };

    /**
     * This will sort the orders in an ascending timestamp order.
     */
    public static Comparator<Order> orderDeliveredComparator = new Comparator<Order>() {
        @Override
        public int compare(Order o1, Order o2) {
            String orderDelivered1 = o1.getDeliveredAt().toUpperCase();
            String orderDelivered2 = o2.getDeliveredAt().toUpperCase();

            return orderDelivered2.compareTo(orderDelivered1);
        }
    };

    /**
     * Concatenates a string with the full delivery address
     * @return a string containing the full delivery address (street address, zip code and city)
     */
    public String getFullOrderAdress() {
        return deliveryStreetAdress + "+" + deliveryZipCode + "+" + deliveryCity;
    }


    @Override
    public void populate(JSONObject data) {
        this.setId(data.optInt("id"));
        this.setEmployeeId(data.optInt("employee_id"));
        this.setCreatedAt(data.optString("created_at"));
        this.setDeliveredAt(data.optString("delivered_at"));
        this.setDeliveryEmailAdress(data.optString("delivery_email"));
        this.setDeliveryPhoneNumber(data.optString("delivery_phone"));
        this.setDeliveryStreetAdress(data.optString("delivery_street_adress"));
        this.setDeliveryCity(data.optString("delivery_city"));
        this.setDeliveryZipCode(data.optInt("delivery_zip"));
        this.setDeliveryLatitude(data.optString("delivery_latitude"));
        this.setDeliveryLongitude(data.optString("delivery_longitude"));
    }
}
