package com.example.iths.fuckingamazingprojectbro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.activity.interfaces.EmployeeOrdersServiceCallback;
import com.example.iths.fuckingamazingprojectbro.data.Order;
import com.example.iths.fuckingamazingprojectbro.helper.MyCustomAdapterDeliveredOrders;
import com.example.iths.fuckingamazingprojectbro.service.EmployeeOrdersService;
import com.example.iths.fuckingamazingprojectbro.service.LoginService;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class displays the specific orders to be delivered for the corresponding driver.
 * This class enables the driver to deliver orders and collect new orders.
 */
public class DeliveredOrdersActivity extends BaseActivity implements EmployeeOrdersServiceCallback {
    private EmployeeOrdersService employeeOrdersService;

    private ArrayList<Order> deliveredOrders;

    private MyCustomAdapterDeliveredOrders customAdapter;
    private ListView listViewDeliveredOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivered_orders);

        //Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.menu_logo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        deliveredOrders = new ArrayList<Order>();


        this.employeeOrdersService = new EmployeeOrdersService(this);

        int id = LoginService.getCurrentEmployee().getId();
        this.employeeOrdersService.get(id, 0, EmployeeOrdersService.FETCHMODE_ONLY_DELIVERED);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handles actionbar clicks here
        int id = item.getItemId();

        // Quit this activity and log out
        if (id == R.id.action_logout) {
            BaseActivity.logout(this);
        }

        // Go back to the main activity
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public void EmployeeOrdersServiceGetFailed(Exception e) {

    }

    @Override
    public void EmployeeOrdersServiceGetSuccess(ArrayList<Order> orders) {
        if(orders.isEmpty()){
            Toast.makeText(DeliveredOrdersActivity.this, getString(R.string.no_orders), Toast.LENGTH_SHORT).show();
        }
        else {
            this.deliveredOrders = orders;

            Collections.sort(this.deliveredOrders, Order.orderIdComparator);
            Collections.sort(this.deliveredOrders, Order.orderAddressComparator);
            Collections.sort(this.deliveredOrders, Order.orderZipComparator);
            Collections.sort(this.deliveredOrders, Order.orderDeliveredComparator);

            listViewDeliveredOrders = (ListView) findViewById(R.id.ListViewDeliveredOrders);
            customAdapter = new MyCustomAdapterDeliveredOrders(this, this.deliveredOrders);
            listViewDeliveredOrders.setAdapter(customAdapter);
            listViewDeliveredOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("order", deliveredOrders.get(position));
                    Intent intent = new Intent(DeliveredOrdersActivity.this, OrderDetailsActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);  //This intent takes you to orderDetailsActivity and adds corresponding order as serializable...
                }
            });
        }
    }
}
