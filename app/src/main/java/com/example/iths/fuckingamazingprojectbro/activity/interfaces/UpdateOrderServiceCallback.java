package com.example.iths.fuckingamazingprojectbro.activity.interfaces;

/**
 * Created by iths on 2015-12-07.
 */
public interface UpdateOrderServiceCallback {
    public void UpdateOrderServiceFailed(Exception e);
    public void UpdateOrderServiceSuccess(String message);
}
