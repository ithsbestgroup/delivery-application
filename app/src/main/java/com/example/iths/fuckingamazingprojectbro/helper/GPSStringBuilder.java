package com.example.iths.fuckingamazingprojectbro.helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

/**
 * Class for creating strings for the GPS for navigation.
 */
public class GPSStringBuilder {

    /**
     * Builds a string from a LatLng and the destination
     * @param origin the GPS position from where you are
     * @param destination your destination
     */

    public void buildString(LatLng origin, String destination) {
        String lat = String.valueOf(origin.latitude);
        String lng = String.valueOf(origin.longitude);
        String originAsString = lat+","+lng;

        String requestString = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+originAsString+"&destinations="+destination;

        try {
            URL requestUrl = new URL(requestString);
            URLConnection connection = requestUrl.openConnection();

            InputStream inputStream = connection.getInputStream();

            InputStreamReader i = new InputStreamReader(inputStream, "UTF-8");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}