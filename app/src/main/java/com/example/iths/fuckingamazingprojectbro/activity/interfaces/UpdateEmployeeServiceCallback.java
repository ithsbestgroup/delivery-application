package com.example.iths.fuckingamazingprojectbro.activity.interfaces;

/**
 * Created by iths on 2015-12-07.
 */
public interface UpdateEmployeeServiceCallback {
    public void UpdateEmployeeServiceFailed(Exception e);
    public void UpdateEmployeeServiceSuccess(String message);
}
