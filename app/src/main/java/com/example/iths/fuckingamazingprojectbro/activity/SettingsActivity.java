package com.example.iths.fuckingamazingprojectbro.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.iths.fuckingamazingprojectbro.R;
import com.example.iths.fuckingamazingprojectbro.activity.interfaces.UpdateEmployeeServiceCallback;
import com.example.iths.fuckingamazingprojectbro.service.LoginService;
import com.example.iths.fuckingamazingprojectbro.service.UpdateEmployeeService;

/**
 * The user will use this activity to change settings in the application.
 */
public class SettingsActivity extends BaseActivity implements UpdateEmployeeServiceCallback {

    /**
     * This is an id for when you want to change user settings.
     */
    public static final String USER_SETTINGS = "USER_SETTINGS";

    private SeekBar seekBar;
    private int seekBarValue;
    private TextView textView;
    private SharedPreferences sh;
    private UpdateEmployeeService updateEmployeeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initializeVariables();

        this.updateEmployeeService = new UpdateEmployeeService(this);

        sh = getSharedPreferences(USER_SETTINGS, MODE_PRIVATE);

        // Sets up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.menu_logo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Sets the seekbar to the users previous settings
        int ordersInBatchSetting = Integer.parseInt(
                LoginService.getCurrentEmployee().getSetting(UpdateEmployeeService.SETTING_ORDERS_IN_BATCH));

        seekBarValue = ordersInBatchSetting;
        seekBar.setProgress(seekBarValue);

        // Also the text view gets this value
        textView.setText(String.valueOf(seekBarValue));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = seekBarValue;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue+1;
                textView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarValue = progress;

                String id = String.valueOf(LoginService.getCurrentEmployee().getId());
                String settingName = UpdateEmployeeService.SETTING_ORDERS_IN_BATCH;
                String value = String.valueOf(seekBarValue);

                LoginService.getCurrentEmployee().setSetting(settingName, value);
                updateEmployeeService.setting(id, settingName, value);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.generic_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        // Go back to the main activity
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        // Quit this activity and log out
        if(id == R.id.action_logout) {
            BaseActivity.logout(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Initializes visual objects.
     */
    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seek_bar_download_orders_amount);
        textView = (TextView) findViewById(R.id.text_view_download_orders_amount);
    }

    @Override
    public void UpdateEmployeeServiceFailed(Exception e) {

    }

    @Override
    public void UpdateEmployeeServiceSuccess(String message) {
        Toast.makeText(SettingsActivity.this, R.string.setting_updated, Toast.LENGTH_SHORT).show();
    }
}
